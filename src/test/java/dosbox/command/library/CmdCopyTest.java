/*
 * DOSBox, Scrum.org, Professional Scrum Developer Training
 * Authors: Rainer Grau, Daniel Tobler, Zuehlke Technology Group
 * Copyright (c) 2013 All Right Reserved
 */ 

package dosbox.command.library;

import org.junit.Before;
import org.junit.Test;

import dosbox.command.library.CmdCd;
import dosbox.filesystem.Directory;
import dosbox.filesystem.File;
import dosbox.filesystem.FileSystemItem;
import dosbox.helpers.TestHelper;

public class CmdCopyTest extends CmdTest {

	@Before
    public void setUp()
    {
        // Check this file structure in base class: crucial to understand the tests.
        this.createTestFileStructure();

        // Add all commands which are necessary to execute this unit test
        // Important: Other commands are not available unless added here.
        commandInvoker.addCommand(new CmdCopy("copy", drive));
        
        //stub directory and files        
        Directory dirA = new Directory("a");
        dirA.add(new File("test1", ""));
        dirA.add(new File("test2", ""));
        
        Directory dirB = new Directory("b");
        //dirB.add(new File("test2", ""));
        
        drive.getCurrentDirectory().add(dirA);
        drive.getCurrentDirectory().add(dirB); 
        
        drive.changeCurrentDirectory(dirA);
    }

    @Test
    public void CmdCopy_fileNotFound_outputterShowFileNotFound()
    {    
        executeCommand("copy test ../b");
        TestHelper.assertContains("File not found!",testOutput);
    }
    
    @Test
    public void CmdCopy_destinationNotFound_outputterShowDestinationNotFound()
    {    
        executeCommand("copy test1 ../x");
        TestHelper.assertContains("Destination",testOutput);
    }
    
    @Test
    public void CmdCopy_fileInDestinationNotExists_ok()
    {    
        executeCommand("copy test2 ../b");
        TestHelper.assertOutputIsEmpty(testOutput);
    }
    
    @Test
    public void CmdCopy_fileInDestinationHaveExists_ok()
    {    
    	Directory tempDestinationDirectory = (Directory)drive.getItemFromPath("../b");
    	tempDestinationDirectory.add(new File("test2", ""));
    	
        executeCommand("copy test2 ../b");
        TestHelper.assertOutputIsEmpty(testOutput);
    }
    
}
