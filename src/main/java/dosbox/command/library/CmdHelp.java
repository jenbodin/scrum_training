/*
 * DOSBox, Scrum.org, Professional Scrum Developer Training
 * Authors: Rainer Grau, Daniel Tobler, Zuehlke Technology Group
 * Copyright (c) 2013 All Right Reserved
 */

package dosbox.command.library;

import java.util.HashMap;

import dosbox.command.framework.Command;
import dosbox.interfaces.IDrive;
import dosbox.interfaces.IOutputter;

/**
 * Command to change current directory. Example for a command with optional
 * parameters.
 */
class CmdHelp extends Command {

	private static final String ERR_NO_CMD = "Error: This command is not supported by the help utility.";

	private static final HashMap<String, String> HELP_MAPS = new HashMap<String, String>() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 8958514009948101799L;

		{
			put("dir", "DIR Displays a list of files and subdirectories in a directory.");
			put("cd", "CD Displays the name of or changes the current directory.");
			put("exit", "EXIT Quits the CMD.EXE program (command interpreter).");
			put("format", "FORMAT Formats a disk for use with Windows.");
			put("help", "HELP Provides Help information for Windows commands.");
			put("label", "LABEL Creates, changes, or deletes the volume label of a disk.");
			put("mkdir", "MKDIR Creates a directory.");
			put("mkfile", "MKFILE Created a file.");
			put("move", "MOVE Moves one or more files from one directory to another directory.");
		}
	};

	protected CmdHelp(String name, IDrive drive) {
		super(name, drive);
	}

	@Override
	public boolean checkNumberOfParameters(int numberOfParametersEntered) {
		return (numberOfParametersEntered == 0 || numberOfParametersEntered == 1);
	}

	@Override
	public void execute(IOutputter outputter) {
		if (getParameterCount() == 0) {
			for (String key : HELP_MAPS.keySet()) {
				outputter.printLine(HELP_MAPS.get(key));
			}
		} else {

			String key = getParameterAt(0).toLowerCase();

			outputter.printLine(HELP_MAPS.containsKey(key) ? HELP_MAPS.get(key)
					: ERR_NO_CMD);

		}
	}
}