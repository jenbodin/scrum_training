/*
 * DOSBox, Scrum.org, Professional Scrum Developer Training
 * Authors: Rainer Grau, Daniel Tobler, Zuehlke Technology Group
 * Copyright (c) 2013 All Right Reserved
 */ 

package dosbox.command.library;

import java.util.List;

import dosbox.command.framework.Command;
import dosbox.filesystem.Directory;
import dosbox.filesystem.Drive;
import dosbox.filesystem.File;
import dosbox.filesystem.FileSystemItem;
import dosbox.interfaces.IDrive;
import dosbox.interfaces.IOutputter;

class CmdCopy extends Command {
	private IOutputter outputter;
	

	public CmdCopy(String cmdName, IDrive drive) {
		super(cmdName, drive);
	}

	@Override
	public void execute(IOutputter outputter) {
		this.outputter = outputter;
		String fileName = this.getParameterAt(0);		
		String destination = this.getParameterAt(1);	
		boolean isFileNotExists = true;

		for(FileSystemItem file:this.getDrive().getCurrentDirectory().getContent() ){
			
			if(file.getName().equals(fileName)){
				//System.out.println("copy fileName:"+file.getName());
				copyFileToDestination(file,destination);
				isFileNotExists = false;
				break;
			}			
		}
		
		if(isFileNotExists){
			this.outputter.print("File not found!");
		}
	}
	
	public void copyFileToDestination(FileSystemItem file,String destination){
		Directory destinationDirectory = extractAndCheckIfValidDirectory( destination ,this.getDrive());
		//System.out.println("destination dir :"+destinationDirectory.getName());
		
		if(destinationDirectory != null){
			List<FileSystemItem> destinationFileList = destinationDirectory.getContent();
			
			if(destinationFileList == null || destinationFileList.isEmpty()){
				destinationDirectory.add((File)file);
			}else{
				for(FileSystemItem fileInDestination:destinationFileList ){
					if(fileInDestination.getName().equals(file.getName())){
						//System.out.println("copy exists ");
						destinationDirectory.remove(fileInDestination);
						destinationDirectory.add((File)file);
					}else{
						//System.out.println("copy not exists ");
						destinationDirectory.add((File)file);
					}	
				}
			}
		}
	}
	
	private Directory extractAndCheckIfValidDirectory(String destinationDirectoryName, IDrive drive) {
        FileSystemItem tempDestinationDirectory = drive.getItemFromPath(destinationDirectoryName);
        if (tempDestinationDirectory == null)
        {
        	this.outputter.print("Destination directory not found!");
            return null;
        }
        if (!tempDestinationDirectory.isDirectory())
        {
        	this.outputter.print("Destination is not a directory!");
            return null;
        }
        return (Directory)tempDestinationDirectory;
    }
	
	
}
