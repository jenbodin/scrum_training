package dosbox.command.library;

import org.junit.Before;
import org.junit.Test;

import dosbox.helpers.TestHelper;

public class CmdHelpTest extends CmdTest {

	@Before
	public void setUp() {
        // Check this file structure in base class: crucial to understand the tests.
        this.createTestFileStructure();

		// Add all commands which are necessary to execute this unit test
		// Important: Other commands are not available unless added here.
		this.commandInvoker.addCommand(new CmdHelp("help", this.drive));
	}
	
	@Test
	public void CmdHelp_WithoutParameter_PrintAllCommands() {
		executeCommand("help");
		TestHelper.assertContains("CD Displays the name of or changes the current directory.", testOutput);
		TestHelper.assertContains("DIR Displays a list of files and subdirectories in a directory.", testOutput);
		TestHelper.assertContains("EXIT Quits the CMD.EXE program (command interpreter).", testOutput);
		TestHelper.assertContains("FORMAT Formats a disk for use with Windows.", testOutput);
		TestHelper.assertContains("HELP Provides Help information for Windows commands.", testOutput);
		TestHelper.assertContains("LABEL Creates, changes, or deletes the volume label of a disk.", testOutput);
		TestHelper.assertContains("MKDIR Creates a directory.", testOutput);
		TestHelper.assertContains("MKFILE Created a file.", testOutput);
		TestHelper.assertContains("MOVE Moves one or more files from one directory to another directory.", testOutput);
	}
	
	@Test
	public void CmdHelp_WithCdParameter_PrintDetailOfCdParamterOnly() {
		executeCommand("help CD");
		TestHelper.assertContains("CD Displays the name of or changes the current directory.", testOutput);
		TestHelper.assertNotContains("LABEL Creates, changes, or deletes the volume label of a disk.", testOutput);	
	}
	
	@Test
	public void CmdHelp_WithDirParameter_PrintDetailOfDirParamterOnly() {
		executeCommand("help DIR");
		TestHelper.assertContains("DIR Displays a list of files and subdirectories in a directory.", testOutput);
	}
	
	@Test
	public void CmdHelp_WithExitParameter_PrintDetailOfExitParamterOnly() {
		executeCommand("help EXIT");
		TestHelper.assertContains("EXIT Quits the CMD.EXE program (command interpreter).", testOutput);
	}
	
	@Test
	public void CmdHelp_WithFormatParameter_PrintDetailOfFormatParamterOnly() {
		executeCommand("help FORMAT");
		TestHelper.assertContains("FORMAT Formats a disk for use with Windows.", testOutput);
	}
	
	@Test
	public void CmdHelp_WithHelpParameter_PrintDetailOfHelpParamterOnly() {
		executeCommand("help HELP");
		TestHelper.assertContains("HELP Provides Help information for Windows commands.", testOutput);
	}
	
	@Test
	public void CmdHelp_WithLabelParameter_PrintDetailOfLabelParamterOnly() {
		executeCommand("help LABEL");
		TestHelper.assertContains("LABEL Creates, changes, or deletes the volume label of a disk.", testOutput);
	}
	
	@Test
	public void CmdHelp_WithMkdirParameter_PrintDetailOfMkdirParamterOnly() {
		executeCommand("help MKDIR");
		TestHelper.assertContains("MKDIR Creates a directory.", testOutput);
	}
	
	@Test
	public void CmdHelp_WithMkfileParameter_PrintDetailOfMkfileParamterOnly() {
		executeCommand("help MKFILE");
		TestHelper.assertContains("MKFILE Created a file.", testOutput);
	}
	
	@Test
	public void CmdHelp_WithMoveParameter_PrintDetailOfMoveParamterOnly() {
		executeCommand("help MOVE");
		TestHelper.assertContains("MOVE Moves one or more files from one directory to another directory.", testOutput);
	}
	
	@Test
	public void CmdHelp_WithWrongParameter_PrintErrorMessage() {
		
		executeCommand("help test");

		TestHelper.assertContains("Error: This command is not supported by the help utility.", testOutput);
		TestHelper.assertNotContains("CD Displays the name of or changes the current directory.", testOutput);
		
	}
	
}
